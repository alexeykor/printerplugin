class A {
  void main() {
    try {
      test;
    }
    finally {
      test;
    }

    try(Int a = 5; Vim testVar = new Vim())
    {
      test;
      test;
    }
    finally
    {
      test;
      test;
    }

    try
    {
      test;
      test;
    }
    catch  (Test e)
    {
      test;
      test;
    }
    finally
    {
      test;
      test;
    }

    try
    {
      test;
      test;
    }
    catch(Test e)
    {
      test;
      test;
    }
    catch(Test a)
    {
      test;
      test;
    }

    try
    {
      test;
      test;
    }
    catch(Test e)
    {
      test;
      test;
    }
    catch(Test a)
    {
      test;
      test;
    }
    finally
    {
      test;
      asdfasdl;
    }
  }
}