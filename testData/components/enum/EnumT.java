enum E {
  GET1 {
    int get() { return 1; }
  }
  , GET2("@") {
    int get() { return 2; }
  }
  , GET3("*") {
    int get() { return 3; }
  }
  , GETN("y") {
    int get() { return 10; }
  };

  E(String str) {
    System.out.println(str);
  }

  E(int x) {
    System.out.println(x);
  }

  abstract int get();

  public int a;
}