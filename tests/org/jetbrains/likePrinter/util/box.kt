package org.jetbrains.format.box

import org.junit.Test
import kotlin.test.assertEquals
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.likePrinter.util.box.toBox
import org.jetbrains.format.toFormat

/**
 * User: anlun
 */

public class BoxTest {
    Test fun stringToBox_Empty() {
        val str = ""
        val expected = Box(0, 0)
        val result = str.toBox(1)

        assertEquals(expected, result, "Incorrect Box for empty string.")
    }

    Test fun stringToBox_Test() {
        val str = "Hello\nBox"
        val startLineOffset = 0
        val expected = Box(5, 2)
        val result = str.toBox(startLineOffset)

        assertEquals(expected, result, "Incorrect Box for string.")
    }

    Test fun toBoxTest_2Line() {
        val str    = "Hello\ntest!"
        val box = Box(5, 2)

        val fmt    = str.toFormat()
        val fmtBox = fmt.toBox()

        assertEquals(box, fmtBox, "Incorrect toBox transformation (2 line string).")
    }

    Test fun toBoxTest_Empty() {
        val str = ""
        val box = Box(0, 0)

        val fmt    = str.toFormat()
        val fmtBox = fmt.toBox()

        assertEquals(box, fmtBox, "Incorrect toBox transformation (empty string).")
    }

}