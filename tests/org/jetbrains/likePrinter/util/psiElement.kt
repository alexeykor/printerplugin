package org.jetbrains.likePrinter.util.psiElement

import org.junit.Test
import com.intellij.testFramework.fixtures.LightCodeInsightFixtureTestCase
import com.intellij.openapi.vfs.LocalFileSystem
import java.io.FileInputStream
import java.io.InputStreamReader
import kotlin.test.assertTrue
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import kotlin.test.assertEquals
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.readFile
import org.jetbrains.likePrinter.getClassFromFile
import org.jetbrains.likePrinter.getMethodFromFile

/**
 * User: anlun
 */

public class PsiElementTest: LightCodeInsightFixtureTestCase() {
    /** PsiElement.getCorrectTextOffset() **/

    Test fun testGetCorrectTextOffset_ClassWithoutModifiers() {
        getCorrectTextOffset_Element("class1.java", { s -> getClassFromFile(s, getElementFactory()) })
    }

    Test fun testGetCorrectTextOffset_ClassWithModifiers() {
        getCorrectTextOffset_Element("class2.java", { s -> getClassFromFile(s, getElementFactory()) })
    }

    Test fun testGetCorrectTextOffset_MethodWithReturnType() {
        getCorrectTextOffset_Element("method1.java", { s -> getMethodFromFile(s, getElementFactory()) })
    }

    Test fun testGetCorrectTextOffset_MethodWithoutReturnType() {
        getCorrectTextOffset_Element("method2.java", { s -> getMethodFromFile(s, getElementFactory()) })
    }

    private fun getCorrectTextOffset_Element(
              filename: String
            , getElementWithExpectedOffset: (String) -> Pair<PsiElement, Int>
    ) {
        val (testClass, expected) = getElementWithExpectedOffset("util/" + filename)
        val offset = testClass.getCorrectTextOffset()
        assertEquals(expected, offset, "Incorrect offset for class!")
    }

    Test fun testGetTextWithoutLeadingComments() {
        val (methodFromFile, _) = getMethodFromFile("util/methodC.java", getElementFactory())
        val result = methodFromFile.getTextWithoutLeadingComments()
        val expected = readFile("util/methodC_expected.java")
        assertEquals(expected, result)
    }
}