package org.jetbrains.likePrinter.generators

/**
 * Created by Aleksei on 11/23/2014.
 */

import org.junit.Test
import kotlin.test.*
import org.jetbrains.likePrinter.generators.*
import java.io.File

public class GeneratorsTests {
    Test fun runGenerator() {
        val path = "testData/generators/input/if.xml"
        val parser : StaXParser = StaXParser()
        File("testData/generators/output/out.kt").writeText(parser.readXml(path).toString())
        assertEquals("", "", "Generator doesn't work")
    }

    Test fun generateStatement_noSubtrees() {
        val path = "testData/generators/input/noSubtrees.xml"
        val parser : StaXParser = StaXParser()
        val text = parser.readXml(path).toString()
        assertTrue(!text.contains("_TAG"),"Statement without subtrees wasn't generated correctly")
    }

    Test fun generateStatement_Subtrees_noCodeBlocks() {
        val path = "testData/generators/input/subtree_NoCB.xml"
        val parser : StaXParser = StaXParser()
        val text = parser.readXml(path).toString()
        assertTrue(text.contains("_TAG") && !(text.contains("CodeBlock") || text.contains("CBtoInsertPlaceMap"))
                ,"Statement with subtrees and without CB was't generated correctly")
    }

    Test fun generateStatement_Subtrees_CodeBlocks() {
        val path = "testData/generators/input/subtree_CB.xml"
        val parser : StaXParser = StaXParser()
        val text = parser.readXml(path).toString()
        assertTrue(text.contains("_TAG") && text.contains("CodeBlock") && text.contains("CBtoInsertPlaceMap")
                ,"Statement with subtrees and CB was't generated correctly")
    }
}