package org.jetbrains.likePrinter.components

import org.junit.Test

/**
 * User: anlun
 */
public class TryTest: ComponentTest() {
    override fun getOurPathToComponents(): String = PATH_TO_COMPONENTS + "try/"

    Test fun test_withoutFinally() {
        test("TryT.java", "TryEx.java", "TryExpected.java");
    }

    Test fun test_withFinally() {
        test("TryT.java", "TryEx_1.java", "TryExpected_1.java");
    }

    Test fun test_withResources() {
        test("TryT.java", "TryEx_2.java", "TryExpected_2.java");
    }
}