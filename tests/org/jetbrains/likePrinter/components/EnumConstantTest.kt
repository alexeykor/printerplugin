package org.jetbrains.likePrinter.components

import org.junit.Test

/**
 * User: anlun
 */
public class EnumConstantTest: ComponentTest() {
    override fun getOurPathToComponents(): String = PATH_TO_COMPONENTS + "enum/"

    Test fun test_1() {
        test("EnumT.java", "EnumEx.java", "EnumExpected.java")
    }
}