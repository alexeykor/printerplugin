package org.jetbrains.likePrinter

import java.io.FileInputStream
import java.io.InputStreamReader
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiElementFactory

/**
 * User: anlun
 */

val pathToTestData = "testData/"

fun readFile(filename: String): String {
    val pathToFile = pathToTestData + filename
    val stream = FileInputStream(pathToFile).buffered();
    val content: String
    try {
        val reader = InputStreamReader(stream, "UTF-8");
        content = reader.readText()
    } finally {
        stream.close();
    }

    return content
}

public fun getMethodFromFile(filename: String, factory: PsiElementFactory?): Pair<PsiElement, Int> {
    val content = readFile(filename)
    val method  = factory!!.createMethodFromText(content, null)
    return Pair(method, 0)
}

public fun getClassFromFile(filename: String, factory: PsiElementFactory?): Pair<PsiElement, Int>  {
    val content = readFile(filename)
    val dummyClass     = factory!!.createClassFromText(content, null)
    val dummyClassText = dummyClass.getText()
    val expected       = dummyClassText!!.indexOf("\n") + 1
    val testClass      = dummyClass.getAllInnerClasses()[0]

    return Pair(testClass, expected)
}
