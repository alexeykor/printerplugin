package org.jetbrains.likePrinter.generators

import java.io.File
import java.util.ArrayList
import com.intellij.openapi.util.TextRange
import org.jetbrains.likePrinter.util.base.InsertPlace
import org.jetbrains.likePrinter.util.base.insertFormatsToText
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.util.string.replaceAllInsertPlace

/**
 * Created by Aleksei on 3/5/2015.
 */
public class Component (
        val name : String
        , val psiStatementClass : String
        , val subtrees : List<ComponentSubtree>
        , val isTemplSuit : String
)
{
    override public fun toString(): String {

        val componentCodeTemplate = File("resources/generators/Component.txt").readText()

        val prepSubtrees = {
            (acc: String, subtree: ComponentSubtree) ->
            when(subtree.isCodeBlock) {
                false   ->  acc + "prepare${subtree.name.capitalize()}Variants(p, variants, context)\n"
                else    ->  acc + "preparePossibleCodeBlockPart(p.get${subtree
                        .psiSubtreeGet}(), ${subtree.name.toUpperCase()}_TAG, variants, context)\n"
            }
        }

        val getTags = {
            (acc: String, subtree: ComponentSubtree) ->
            when(subtree.isCodeBlock) {
                false   ->  acc + "if (p.get${subtree.psiSubtreeGet}() != null) { set.add(${subtree
                        .name.toUpperCase()}_TAG) }\n"
                else    ->  acc + "addPossibleCodeBlockTag(set, p.get${subtree.psiSubtreeGet}(), ${subtree
                        .name.toUpperCase()}_TAG)\n"
            }
        }

        val subtreesToInsPlace = {
            (acc: String, subtree: ComponentSubtree) ->
            when (Pair(subtree.isCodeBlock, subtree.isRequired)){
                Pair(false, false) ->
                    acc + "add${subtree.name.capitalize()}ToInsertPlaceMap(newP, insertPlaceMap, negShift)\n"
                Pair(false, true)  ->
                    acc + "if (!add${subtree.name
                            .capitalize()}ToInsertPlaceMap(newP, insertPlaceMap, negShift)) { return null }\n"
                Pair(true, false)  ->
                    acc + "addCBtoInsertPlaceMap(newP.get${subtree
                            .psiSubtreeGet}(), ${subtree.name.toUpperCase()}_TAG, insertPlaceMap, text)\n"
                else                                        ->
                    acc + "if (!addCBtoInsertPlaceMap(newP.get${subtree.psiSubtreeGet}(), ${subtree
                            .name.toUpperCase()}_TAG, insertPlaceMap, text)) { return null }\n"
            }
        }

        val declTags = {
            (acc: String, subtree: ComponentSubtree) ->
            acc + "final val ${subtree.name.toUpperCase()}_TAG: String\n    get() = \"${subtree.name.toLowerCase()}\"\n"
        }

        val genSubtrees = {
            (acc: String, subtree: ComponentSubtree) ->
            when (subtree.isCodeBlock) {
                false    -> acc + subtree.toString()
                else     -> acc
            }
        }

        val parametersList = listOf(
                Pair("@NAME_CC@"        , name.capitalize()                     ),
                Pair("@ST_CLASS@"       , psiStatementClass                     ),
                Pair("@DECL_TAGS@"      , subtrees.fold("", declTags)           ),
                Pair("@GEN_SUBTREES@"   , subtrees.fold("", genSubtrees)        ),
                Pair("@PREP_SUBTREES@"  , subtrees.fold("", prepSubtrees)       ),
                Pair("@GET_TAGS@"       , subtrees.fold("", getTags)            ),
                Pair("@TEMPL_SUIT@"     , isTemplSuit                           ),
                Pair("@ADD_SUBTREES@"   , subtrees.fold("", subtreesToInsPlace) )
        )

        return componentCodeTemplate.replaceAllInsertPlace(parametersList)
    }
}