package org.jetbrains.likePrinter.generators

import java.io.File
import org.jetbrains.likePrinter.util.string.replaceAllInsertPlace

/**
 * Created by Aleksei on 3/5/2015.
 */
public class ComponentSubtree (
        public val name : String
        , val psiStatementClass : String
        , val psiSubtreeGet : String
        , val isEverywhereSuit : Boolean
        , val isCodeBlock : Boolean
        , val isRequired : Boolean
)
{
    override public fun toString(): String{

        val subtreeCodeTemplate = File("resources/generators/ComponentSubtree.txt").readText()

        val everywhereSuit =
                when(isEverywhereSuit) {
                    true    ->  "Box.getEverywhereSuitable())\n"
                    else    ->  "$name.toBox())\n"
                }

        val parametersList = listOf(
                Pair("@NAME_CC@"        , name.capitalize() ),
                Pair("@ST_CLASS@"       , psiStatementClass ),
                Pair("@SUBTREE_GET@"    , psiSubtreeGet     ),
                Pair("@NAME@"           , name              ),
                Pair("@NAME_CAP@"       , name.toUpperCase()),
                Pair("@EVERYWHERE_SUIT@", everywhereSuit    )
        )

        return subtreeCodeTemplate.replaceAllInsertPlace(parametersList)
    }
}