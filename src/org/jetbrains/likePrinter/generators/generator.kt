package org.jetbrains.likePrinter.generators

/**
 * Created by Aleksei on 11/21/2014.
 */

import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.ArrayList
import kotlin.Iterator
import kotlin.List
import kotlin.io.*

import javax.xml.stream.XMLEventReader
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLStreamException
import javax.xml.stream.events.Attribute
import javax.xml.stream.events.EndElement
import javax.xml.stream.events.StartElement
import javax.xml.stream.events.XMLEvent

public class StaXParser {

    var name               : String? = null
    var psiStatementClass  : String? = null
    var isTemplateSuitable : String? = null

class object {
    private fun createSubtreeComponent(name: String?, psiStatementClass: String?, psiGetMethod: String?
                                  , isEveryWhereSuitable: String?, isCodeBlock: String?, isRequired: String?
    ): ComponentSubtree? {
        if (name                 == null) { return null }
        if (psiStatementClass    == null) { return null }
        if (psiGetMethod         == null) { return null }
        if (isEveryWhereSuitable == null) { return null }
        if (isCodeBlock          == null) { return null }
        if (isRequired           == null) { return null }
        return ComponentSubtree(name, psiStatementClass, psiGetMethod, isEveryWhereSuitable.toBoolean()
                , isCodeBlock.toBoolean(), isRequired.toBoolean())
    }

    private fun createComponent(name: String?, psiStatementClass: String?
                           , isTemplateSuitable: String?, subtrees: ArrayList<ComponentSubtree>
    ): Component? {
        if (name               == null) { return null }
        if (psiStatementClass  == null) { return null }
        if (isTemplateSuitable == null) { return null }
        return Component(name, psiStatementClass, subtrees, isTemplateSuitable)
    }

    private fun getEventReader(inputXml: String): XMLEventReader {
        val inputFactory: XMLInputFactory = XMLInputFactory.newInstance()
        val mInput: InputStream = FileInputStream(inputXml)
        return inputFactory.createXMLEventReader(mInput)
    }
}
    private fun getSubtreeComponent(startElement: StartElement, psiStatementClass: String?): ComponentSubtree? {
        var name            : String? = null
        var psiGetMethod    : String? = null
        var isEverywhereSuit: String? = "false"
        var isCodeBlock     : String? = null
        var isRequired      : String? = null;
        val attributes      : Iterator<Any?> = startElement.getAttributes()
        while (attributes.hasNext()) {
            val attribute = attributes.next()
            if (attribute !is Attribute) { break }
            val value = attribute.getValue()
            when (attribute.getName().toString()) {
                "name"             -> name             = value
                "psiGetMethod"     -> psiGetMethod     = value
                "isEverywhereSuit" -> isEverywhereSuit = value
                "isCodeBlock"      -> isCodeBlock      = value
                "isRequired"       -> isRequired       = value
            }
        }
        return createSubtreeComponent(name, psiStatementClass, psiGetMethod
                    , isEverywhereSuit, isCodeBlock, isRequired)
    }

    private fun startElementReader(event: XMLEvent, subtrees: ArrayList<ComponentSubtree>) : Triple<String?, String?, String?> {

        val startElement : StartElement = event.asStartElement()
        val localPart = startElement.getName().getLocalPart()
        if (localPart == "component") {
            val attributes: Iterator<Any?> = startElement.getAttributes()
            while (attributes.hasNext()) {
                val attribute = attributes.next()
                if (attribute !is Attribute) { break }
                val value = attribute.getValue()
                when (attribute.getName().toString()) {
                    "name"              -> name = value
                    "psiStatementClass" -> psiStatementClass = value
                }
            }
        }
        if (localPart == "isTemplSuit") {
            val attributes = startElement.getAttributes()
            val attributeIsTemplSuitCode = attributes.next()
            if (attributeIsTemplSuitCode is Attribute) {
                isTemplateSuitable = attributeIsTemplSuitCode.getValue()
            }
        }
        if (localPart == "subtree") {
            val subtree = getSubtreeComponent(startElement, psiStatementClass)
            if (subtree != null) { subtrees.add(subtree) }
        }
        return Triple(name, psiStatementClass, isTemplateSuitable)
    }

    private fun readXml_exception(inputXml: String): Component? {
        val eventReader = getEventReader(inputXml)

        val subtrees : ArrayList<ComponentSubtree> = ArrayList()

        while (eventReader.hasNext()) {
            val event = eventReader.nextEvent()

            if (event.isStartElement()) {
                val triple = startElementReader(event, subtrees)
                name               = triple.first   ?: name
                psiStatementClass  = triple.second  ?: psiStatementClass
                isTemplateSuitable = triple.third   ?: isTemplateSuitable
            }
            if (event.isEndElement()) {
                val endElement: EndElement = event.asEndElement();
                if (endElement.getName().getLocalPart() != "component") { continue }
                return createComponent(name, psiStatementClass, isTemplateSuitable, subtrees)
            }
        }
        return null
    }

    public fun readXml(inputXml : String) : Component? {
        try {
            return readXml_exception(inputXml)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: XMLStreamException) {
            e.printStackTrace()
        }
        return null
    }
}