package org.jetbrains.likePrinter.components.lists

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiAnnotationParameterList
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiMethod
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import java.util.ArrayList
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.Format
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class AnnotationParameterListComponent(
        printer: Printer
): PsiElementComponent<PsiAnnotationParameterList, SmartInsertPlace, ListTemplate<PsiAnnotationParameterList, SmartInsertPlace>>(printer)
 , ListComponent      <PsiAnnotationParameterList> {

    override public fun getMethodTextFromListElementText(elementText: String): String =
            "@A\n$elementText\nvoid main() {}"

    override public fun getElementFromMethod(method: PsiMethod): PsiAnnotationParameterList? {
        val annotations = method.getModifierList().getAnnotations()
        if (annotations.isEmpty()) { return null }
        return annotations[0].getParameterList()
    }

    override public fun createElement2Tmplt(
              p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                p, list
                , { m ->
                    val elementFromMethod = getElementFromMethod(m)
                    if (elementFromMethod != null) {
                        getList(elementFromMethod)
                    } else {
                        ArrayList<PsiElement>()
                    }
                }
        )
    }

    override protected fun isTemplateSuitable(
            p: PsiAnnotationParameterList, tmplt: ListTemplate<PsiAnnotationParameterList, SmartInsertPlace>
    ): Boolean = true

    override protected fun getElementsVariants(
                    p: PsiAnnotationParameterList
            ,   tmplt: ListTemplate<PsiAnnotationParameterList, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())
    override public fun getSeparator_1(): Format = Format.line(", ")

    override protected fun getList(p: PsiAnnotationParameterList) = p.getAttributes().toList()
}