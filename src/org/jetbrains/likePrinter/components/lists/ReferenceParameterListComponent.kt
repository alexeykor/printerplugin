package org.jetbrains.likePrinter.components.lists

import com.intellij.psi.PsiReferenceParameterList
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import com.intellij.psi.PsiMethod
import java.util.ArrayList
import com.intellij.psi.PsiDeclarationStatement
import com.intellij.psi.PsiLocalVariable
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ReferenceParameterListComponent(
        printer: Printer
): PsiElementComponent<PsiReferenceParameterList, SmartInsertPlace, ListTemplate<PsiReferenceParameterList, SmartInsertPlace>>(printer)
 , ListComponent      <PsiReferenceParameterList> {

    override public fun getMethodTextFromListElementText(elementText: String): String =
        "void main() {A \n$elementText\n variable;}"

    override public fun getElementFromMethod(method: PsiMethod): PsiReferenceParameterList? {
        try {
            val statements = method.getBody()?.getStatements()
            val statement = statements?.get(0) as? PsiDeclarationStatement
            val declaredElements = statement?.getDeclaredElements()
            val variableDeclaration = declaredElements?.get(0) as? PsiLocalVariable
            val referenceElement = variableDeclaration?.getTypeElement()?.getInnermostComponentReferenceElement()
            return referenceElement?.getParameterList()
        } catch (e: IndexOutOfBoundsException) {
            return null
        }
    }

    override public fun createElement2Tmplt(
              p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                p, list
                , { m ->
                    val elementFromMethod = getElementFromMethod(m)
                    if (elementFromMethod != null) {
                        getList(elementFromMethod)
                    } else {
                        ArrayList<PsiElement>()
                    }
                }
        )
    }

    override protected fun isTemplateSuitable(
            p: PsiReferenceParameterList, tmplt: ListTemplate<PsiReferenceParameterList, SmartInsertPlace>
    ): Boolean = true

    override protected fun getElementsVariants(
                    p: PsiReferenceParameterList
            ,   tmplt: ListTemplate<PsiReferenceParameterList, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())

    override protected fun getList(p: PsiReferenceParameterList) = p.getTypeParameterElements().toList()
}