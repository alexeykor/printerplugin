package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiAnnotation
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiElementFactory
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class AnnotationComponent(
        printer: Printer
): PsiElementComponent <PsiAnnotation, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , EmptyUpdateComponent<PsiAnnotation, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    final val PARAMETER_LIST_TAG : String
        get() = "parameter list"

    final val NAME_REFERENCE_TAG: String
        get() = "name reference"

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiAnnotation? {
        val methodText = "$text\nvoid main() {}"
        val method = elementFactory.createMethodFromText(methodText, null)
        val annotations = method.getModifierList().getAnnotations()
        if (annotations.isEmpty()) { return null }
        return annotations[0]
    }

    override public fun getTemplateFromElement(newP: PsiAnnotation): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset()

        val nameReference = newP.getNameReferenceElement()
        if (nameReference != null) {
            insertPlaceMap.put(NAME_REFERENCE_TAG, nameReference.toSmartInsertPlace().shiftRight(negShift))
        }
        val parameterList = newP.getParameterList()
        if (hasElement(parameterList)) {
            insertPlaceMap.put(PARAMETER_LIST_TAG, parameterList.toSmartInsertPlace().shiftRight(negShift))
        }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    protected fun getNameReferenceVariants(p: PsiAnnotation, context: VariantConstructionContext): FormatSet {
        val nameReference = p.getNameReferenceElement()
        if (nameReference == null) { return printer.getEmptySet() }
        return printer.getVariants(nameReference, context)
    }

    protected fun getParameterListVariants(p: PsiAnnotation, context: VariantConstructionContext): FormatSet {
        val parameterList = p.getParameterList()
        return printer.getVariants(parameterList, context)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiAnnotation
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        val nameReferenceVariants = getNameReferenceVariants(p, context)
        if (!nameReferenceVariants.isEmpty()) {
            variants.put(NAME_REFERENCE_TAG, nameReferenceVariants)
        }
        val parameterListVariants = getParameterListVariants(p, context)
        if (!parameterListVariants.isEmpty()) {
            variants.put(PARAMETER_LIST_TAG, parameterListVariants)
        }

        return variants
    }

    override protected fun getTags(p: PsiAnnotation): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getParameterList       ())) { set.add(PARAMETER_LIST_TAG) }
        if (hasElement(p.getNameReferenceElement())) { set.add(NAME_REFERENCE_TAG) }

        return set
    }

    override protected fun isTemplateSuitable(p: PsiAnnotation, tmplt: PsiTemplate<SmartInsertPlace>): Boolean = true
}