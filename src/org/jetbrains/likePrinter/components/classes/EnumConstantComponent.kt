package org.jetbrains.likePrinter.components.classes

import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiEnumConstant
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiClass
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import org.jetbrains.likePrinter.components.NameOwner
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.psi.PsiEnumConstantInitializer
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.util.string.LineEquation
import org.jetbrains.likePrinter.util.string.TagPlaceLine
import org.jetbrains.likePrinter.util.psiElement.hasElement
import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class EnumConstantComponent(
        printer: Printer
): PsiElementComponent<PsiEnumConstant, SmartInsertPlace, PsiTemplateGen<PsiEnumConstant, SmartInsertPlace>>(printer)
 , NameOwner          <PsiEnumConstant, SmartInsertPlace, PsiTemplateGen<PsiEnumConstant, SmartInsertPlace>>
 , ArgumentListOwner  <PsiEnumConstant, SmartInsertPlace, PsiTemplateGen<PsiEnumConstant, SmartInsertPlace>> {
    final val ENUM_INITIALIZER_TAG: String
        get() = "enum initializer"

    private class EnumWithInitializerTemplate(
      psi           : PsiEnumConstant
    , insertPlaceMap: Map<String, SmartInsertPlace>
    , tagPlaceToLineNumberMap: Map<TagPlaceLine, Int>
    , lineEquationMap        : Map<Int, LineEquation>
    , val enumInitializerTemplate: PsiTemplateGen<PsiEnumConstantInitializer, SmartInsertPlace>
    ): PsiTemplateGen<PsiEnumConstant, SmartInsertPlace>(psi, insertPlaceMap, tagPlaceToLineNumberMap, lineEquationMap) {
        override public fun toString(): String {
            return super.toString() + enumInitializerTemplate.toString()
        }
    }

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiEnumConstant? {
        val expressionText = "enum T{$text\n}"
        val newDummyClass = elementFactory.createClassFromText(expressionText, null)
        if (newDummyClass !is PsiClass) { return null }
        val newEnum = newDummyClass.getAllInnerClasses()[0]
        val newP = newEnum.getFields()[0]
        if (newP !is PsiEnumConstant) { return null }
        return newP
    }

    override public fun getTemplateFromElement(newP: PsiEnumConstant): PsiTemplateGen<PsiEnumConstant, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val negShift = -newP.getCorrectTextOffset()

        addArgumentListToInsertPlaceMap(newP, insertPlaceMap, negShift)
        addNameToInsertPlaceMap        (newP, insertPlaceMap, negShift)

        val initializingClass = newP.getInitializingClass()
        if (initializingClass == null) {
            val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
            return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
        }
        insertPlaceMap.put(ENUM_INITIALIZER_TAG, initializingClass.toSmartInsertPlace().shiftRight(negShift))

        val initializingTmplt = printer.enumConstantInitializerComponent.getTmplt(initializingClass)
        if (initializingTmplt == null) { return null }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return EnumWithInitializerTemplate(
                  newP, insertPlaceMap
                , contentRelation.first, contentRelation.second
                , initializingTmplt
        )
    }

    override protected fun prepareSubtreeVariants(p: PsiEnumConstant, context: VariantConstructionContext): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareNameVariants        (p, variants, context)
        prepareArgumentListVariants(p, variants, context)

        return variants
    }

    override protected fun updateSubtreeVariants(
              p       : PsiEnumConstant
            , tmplt   : PsiTemplateGen<PsiEnumConstant, SmartInsertPlace>
            , variants: Map<String, FormatSet>
            , context : VariantConstructionContext
    ): Map<String, FormatSet> {
        val newVariants = HashMap<String, FormatSet>(variants)
        //variants.toMap(newVariants)

        val initializerVariants = getEnumInitializerVariants(p, tmplt, context)
        if (initializerVariants.isEmpty()) { return newVariants }
        newVariants.put(ENUM_INITIALIZER_TAG, initializerVariants)

        return newVariants
    }

    private fun getEnumInitializerVariants(
              p      : PsiEnumConstant
            , tmplt  : PsiTemplateGen<PsiEnumConstant, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet {
        val initializingClass = p.getInitializingClass()
        if (initializingClass == null) { return printer.getEmptySet() }
        if (tmplt !is EnumWithInitializerTemplate) { return printer.getEmptySet() }

        val initializerVariants =
                printer.enumConstantInitializerComponent.getVariantsByTemplate(
                        initializingClass, tmplt.enumInitializerTemplate, context
                )
        return initializerVariants
    }

    override protected fun getTags(p: PsiEnumConstant): Set<String> {
        val set = HashSet<String>()

        set.add(NAME_TAG)
        if (hasElement(p.getInitializingClass())) { set.add(ENUM_INITIALIZER_TAG) }
        if (hasElement(p.getArgumentList     ())) { set.add(   ARGUMENT_LIST_TAG) }

        return set
    }

    override protected fun isTemplateSuitable(p: PsiEnumConstant, tmplt: PsiTemplateGen<PsiEnumConstant, SmartInsertPlace>): Boolean {
        if (!isTemplateSuitable_ArgumentListOwner(p, tmplt)) { return false }
        if ((p.getInitializingClass() != null) != (tmplt.insertPlaceMap.get(ENUM_INITIALIZER_TAG) != null)) { return false }

        return true
    }
}