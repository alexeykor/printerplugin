package org.jetbrains.likePrinter.components.classes

import com.intellij.psi.PsiClassInitializer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.components.ModifierListOwner
import com.intellij.psi.PsiElementFactory
import java.util.HashMap

import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.emptyFormatList
import java.util.HashSet
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent.CodeBlockSpecialTemplateInsertPlace
import com.intellij.psi.PsiCodeBlock
import com.intellij.psi.PsiBlockStatement
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ClassInitializerComponent(
        printer: Printer
): PsiElementComponent <PsiClassInitializer, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , EmptyUpdateComponent<PsiClassInitializer, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ModifierListOwner   <PsiClassInitializer, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiClassInitializer? {
        val newClass = elementFactory.createClassFromText(text, null)
        val initializers = newClass.getInitializers()
        val newP = initializers[0]
        return newP
    }

    override public fun getTemplateFromElement(newP: PsiClassInitializer): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val text = newP.getText() ?: ""

        val negOffset = -newP.getTextOffset()
        addModifierListToInsertPlaceMap(newP, insertPlaceMap, negOffset)
        addCBtoInsertPlaceMap(newP.getBody(), BODY_TAG, insertPlaceMap, text, negOffset)

        val contentRelation = getContentRelation(text, insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(p: PsiClassInitializer, context: VariantConstructionContext): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        prepareModifierListVariants(p, variants, context)
        preparePossibleCodeBlockPart(p.getBody(), BODY_TAG, variants, context)
        return variants
    }

    override protected fun getTags(p: PsiClassInitializer): Set<String> {
        val set = HashSet<String>()
        if (hasElement(p.getModifierList())) { set.add(MODIFIER_LIST_TAG) }
        addPossibleCodeBlockTag(set, p.getBody(), BODY_TAG)
        return set
    }

    override protected fun isTemplateSuitable(p: PsiClassInitializer, tmplt: PsiTemplate<SmartInsertPlace>) = true
}