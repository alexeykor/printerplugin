package org.jetbrains.likePrinter.components.classes

import com.intellij.psi.PsiEnumConstantInitializer
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiEnumConstant
import java.util.HashMap

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class EnumConstantInitializerComponent(
        printer: Printer
): PsiElementComponent<PsiEnumConstantInitializer, SmartInsertPlace, PsiTemplateGen<PsiEnumConstantInitializer, SmartInsertPlace>>(printer)
 , ClassBodyOwner     <PsiEnumConstantInitializer, SmartInsertPlace, PsiTemplateGen<PsiEnumConstantInitializer, SmartInsertPlace>> {

    public fun getVariantsByTemplate(
              p      : PsiEnumConstantInitializer
            , tmplt  : PsiTemplateGen<PsiEnumConstantInitializer, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet {
        val newContext = VariantConstructionContext(printer.getCommentContext(p), context.widthToSuit)
        val variants = updateSubtreeVariants(p, tmplt, HashMap(), newContext)

        var resultVariants = getVariants(tmplt.text, tmplt.insertPlaceMap, variants) ?: printer.getEmptySet()
        resultVariants = printer.surroundVariantsByAttachedComments(p, resultVariants, newContext)
        resultVariants = printer.surroundVariantsByAttachedComments(p, resultVariants, context)
        return resultVariants
    }

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiEnumConstantInitializer? {
        val expressionText = "enum T{AA$text\n}"
        val newDummyClass = elementFactory.createClassFromText(expressionText, null)
        if (newDummyClass !is PsiClass) { return null }
        val newEnum = newDummyClass.getAllInnerClasses()[0]
        val newEnumElement = newEnum.getFields()[0]
        if (newEnumElement !is PsiEnumConstant) { return null }
        val newP = newEnumElement.getInitializingClass()
        return newP
    }

    override public fun getTemplateFromElement(
            newP: PsiEnumConstantInitializer
    ): PsiTemplateGen<PsiEnumConstantInitializer, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        addClassBodyToInsertPlaceMap(newP, insertPlaceMap)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiEnumConstantInitializer
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        val bodyVariants = getBodyVariants(p, /*tmplt,*/ context)
        variants.put(CLASS_BODY_TAG, bodyVariants)
        return variants
    }

    override protected fun getTags(p: PsiEnumConstantInitializer) = setOf(CLASS_BODY_TAG)
    override protected fun isTemplateSuitable(
            p: PsiEnumConstantInitializer, tmplt: PsiTemplateGen<PsiEnumConstantInitializer, SmartInsertPlace>
    ) = true
}