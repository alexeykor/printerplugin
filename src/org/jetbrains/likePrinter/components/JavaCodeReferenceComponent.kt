package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiJavaCodeReferenceElement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiElementFactory
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.util.psiElement.hasElement
import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class JavaCodeReferenceComponent(
        printer: Printer
): PsiElementComponent <PsiJavaCodeReferenceElement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , QualifierOwner      <PsiJavaCodeReferenceElement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , NameOwner           <PsiJavaCodeReferenceElement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiJavaCodeReferenceElement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    final val REFERENCE_PARAMETER_LIST_TAG: String
        get() = "reference parameter list"

    // NAME_TAG from NameOwner for ReferenceNameElement

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiJavaCodeReferenceElement? {
        val fieldText = "$text\na;"
        val field = elementFactory.createFieldFromText(fieldText, null)
        val reference = field.getTypeElement()?.getInnermostComponentReferenceElement()
        return reference
    }

    override public fun getTemplateFromElement(newP: PsiJavaCodeReferenceElement): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset()

             addNameToInsertPlaceMap(newP, insertPlaceMap, negShift)
        addQualifierToInsertPlaceMap(newP, insertPlaceMap, negShift)

        val parameterList = newP.getParameterList()
        if (parameterList != null && hasElement(parameterList)) {
            insertPlaceMap.put(REFERENCE_PARAMETER_LIST_TAG, parameterList.toSmartInsertPlace().shiftRight(negShift))
        }

        if (insertPlaceMap.keySet().equals(setOf(NAME_TAG))) { return null }
        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)

        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    protected fun getReferenceParameterListReferenceVariants(
            p: PsiJavaCodeReferenceElement, context: VariantConstructionContext
    ): FormatSet {
        val parameterList = p.getParameterList()
        if (parameterList == null || !hasElement(parameterList)) { return printer.getEmptySet() }
        return printer.getVariants(parameterList, context)
    }

    override protected fun prepareSubtreeVariants(
                    p: PsiJavaCodeReferenceElement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

             prepareNameVariants(p, variants, context)
        prepareQualifierVariants(p, variants, context)

        val parameterListReferenceVariants = getReferenceParameterListReferenceVariants(p, context)
        if (parameterListReferenceVariants.isNotEmpty()) {
            variants.put(REFERENCE_PARAMETER_LIST_TAG, parameterListReferenceVariants)
        }

        return variants
    }

    override protected fun getTags(p: PsiJavaCodeReferenceElement): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getParameterList       ())) { set.add(REFERENCE_PARAMETER_LIST_TAG) }
        if (hasElement(p.getReferenceNameElement())) { set.add(                    NAME_TAG) }
        if (hasElement(p.getQualifier           ())) { set.add(               QUALIFIER_TAG) }

        return set
    }

    override protected fun isTemplateSuitable(
            p: PsiJavaCodeReferenceElement, tmplt: PsiTemplate<SmartInsertPlace>
    ): Boolean = true
}