package org.jetbrains.likePrinter.components.variables

import org.jetbrains.likePrinter.printer.Printer

/**
 * User: anlun
 */
public class ResourceVariableComponent(
        printer: Printer
): VariableComponent(printer) {
    override protected fun getTemplates() = templates_1
}