package org.jetbrains.likePrinter.components.variables

import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiParameter
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.components.ModifierListOwner
import org.jetbrains.likePrinter.components.NameOwner
import com.intellij.psi.PsiElementFactory
import com.intellij.util.IncorrectOperationException
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.components.TypeOwner

import java.util.HashSet
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ParameterComponent(
        printer: Printer
): PsiElementComponent <PsiParameter, SmartInsertPlace, PsiTemplateGen<PsiParameter, SmartInsertPlace>>(printer)
 , NameOwner           <PsiParameter, SmartInsertPlace, PsiTemplateGen<PsiParameter, SmartInsertPlace>>
 , ModifierListOwner   <PsiParameter, SmartInsertPlace, PsiTemplateGen<PsiParameter, SmartInsertPlace>>
 , TypeOwner           <PsiParameter, PsiTemplateGen<PsiParameter, SmartInsertPlace>>
 , EmptyUpdateComponent<PsiParameter, SmartInsertPlace, PsiTemplateGen<PsiParameter, SmartInsertPlace>> {

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiParameter? {
        try {
            return elementFactory.createParameterFromText(text, null)
        } catch (e: IncorrectOperationException) {
            return null
        }
    }

    override public fun getTemplateFromElement(newP: PsiParameter): PsiTemplateGen<PsiParameter, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset()
                addNameToInsertPlaceMap(newP, insertPlaceMap, negShift)
                addTypeToInsertPlaceMap(newP, insertPlaceMap, negShift)
        addModifierListToInsertPlaceMap(newP, insertPlaceMap, negShift)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiParameter
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

                prepareNameVariants(p, variants, context)
                prepareTypeVariants(p, variants, context)
        prepareModifierListVariants(p, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiParameter): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getTypeElement ())) { set.add(TYPE_TAG         ) }
        set.add(NAME_TAG)
        if (hasElement(p.getModifierList())) ( set.add(MODIFIER_LIST_TAG) )


        return set
    }

    override protected fun isTemplateSuitable(
            p: PsiParameter, tmplt: PsiTemplateGen<PsiParameter, SmartInsertPlace>
    ): Boolean {
        return (p.getTypeElement() != null) == (tmplt.psi.getTypeElement() != null)
    }
}