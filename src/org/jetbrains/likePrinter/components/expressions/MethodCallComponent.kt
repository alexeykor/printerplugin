package org.jetbrains.likePrinter.components.expressions

import com.intellij.psi.PsiMethodCallExpression
import com.intellij.psi.PsiTypeCastExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.components.EmptyUpdateComponent

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.components.classes.ArgumentListOwner
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class MethodCallComponent(
        printer: Printer
): PsiElementComponent <PsiMethodCallExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiMethodCallExpression, PsiTemplate<SmartInsertPlace>>
 , ArgumentListOwner   <PsiMethodCallExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiMethodCallExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val EXPRESSION_TAG: String
        get() = "expression"

    protected fun addExpressionToInsertPlaceMap(
              p: PsiMethodCallExpression
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val qualifierSIP = p.getMethodExpression().toSmartInsertPlace()
        insertPlaceMap.put(EXPRESSION_TAG, qualifierSIP)
        return true
    }

    protected fun prepareExpressionVariants(
              p: PsiMethodCallExpression
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val expressionVariants = getExpressionVariants(p, context)
        if (expressionVariants.isEmpty()) { return }
        variants.put(EXPRESSION_TAG, expressionVariants)
    }

    protected fun getExpressionVariants(
            p: PsiMethodCallExpression, context: VariantConstructionContext
    ): FormatSet {
        val expression = p.getMethodExpression()
        val variants = printer.getVariants(expression, context)
        return variants
    }

    override public fun getTemplateFromElement(newP: PsiMethodCallExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

          addExpressionToInsertPlaceMap(newP, insertPlaceMap, 0)
        addArgumentListToInsertPlaceMap(newP, insertPlaceMap, 0)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiMethodCallExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

          prepareExpressionVariants(p, variants, context)
        prepareArgumentListVariants(p, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiMethodCallExpression) = setOf(EXPRESSION_TAG, ARGUMENT_LIST_TAG)
    override protected fun isTemplateSuitable(p: PsiMethodCallExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}