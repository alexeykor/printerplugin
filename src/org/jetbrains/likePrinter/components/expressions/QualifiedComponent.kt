package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiQualifiedExpression
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.QualifierOwner
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import java.util.HashMap

import com.intellij.openapi.util.TextRange
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

public class QualifiedComponent(
        printer: Printer
): PsiElementComponent <PsiQualifiedExpression, SmartInsertPlace, Template<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiQualifiedExpression, Template<SmartInsertPlace>>
 , QualifierOwner      <PsiQualifiedExpression, SmartInsertPlace, Template<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiQualifiedExpression, SmartInsertPlace, Template<SmartInsertPlace>> {

    override public fun getTemplateFromElement(newP: PsiQualifiedExpression): Template<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        addQualifierToInsertPlaceMap(newP, insertPlaceMap, 0)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiQualifiedExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        prepareQualifierVariants(p, variants, context)
        return variants
    }

    override protected fun getTags(p: PsiQualifiedExpression) = setOf(QUALIFIER_TAG)
    override protected fun isTemplateSuitable(p: PsiQualifiedExpression, tmplt: Template<SmartInsertPlace>) = true

    override protected fun getTemplates() = listOf(getTemplate())

    private fun getTemplate(): Template<SmartInsertPlace> {
        val insertPlaceMap = mapOf(Pair(QUALIFIER_TAG, SmartInsertPlace(TextRange(0, 1), 0, Box.getEverywhereSuitable())))
        val contentRelation = getContentRelation("_", insertPlaceMap)
        return Template("_", insertPlaceMap, contentRelation.first, contentRelation.second)
    }
}