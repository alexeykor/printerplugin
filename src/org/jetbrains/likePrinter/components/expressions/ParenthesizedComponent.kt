package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiParenthesizedExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ParenthesizedComponent(
        printer: Printer
): PsiElementComponent <PsiParenthesizedExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiParenthesizedExpression, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiParenthesizedExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val EXPRESSION_TAG: String
        get() = "expression"

    override public fun getTemplateFromElement(newP: PsiParenthesizedExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val expression = newP.getExpression()
        if (expression == null) { return null }
        insertPlaceMap.put(EXPRESSION_TAG, expression.toSmartInsertPlace())

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiParenthesizedExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        val expressionVariants = getExpressionVariants(p, context)
        if (expressionVariants.isEmpty()) { return variants }
        variants.put(EXPRESSION_TAG, expressionVariants)

        return variants
    }

    protected fun getExpressionVariants(
            p: PsiParenthesizedExpression, context: VariantConstructionContext
    ): FormatSet {
        val expression = p.getExpression()
        if (expression == null) { return printer.getEmptySet() }
        return printer.getVariants(expression, context)
    }

    override protected fun getTags(p: PsiParenthesizedExpression) = setOf(EXPRESSION_TAG)
    override protected fun isTemplateSuitable(p: PsiParenthesizedExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}