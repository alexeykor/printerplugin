package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiLambdaExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiElementFactory
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace

import com.intellij.psi.PsiCodeBlock
import com.intellij.psi.PsiBlockStatement
import org.jetbrains.likePrinter.components.ParameterListOwner
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.components.lists.TypeParameterListComponent
import java.util.HashSet
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class LambdaComponent(
        printer: Printer
): PsiElementComponent <PsiLambdaExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiLambdaExpression, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiLambdaExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ParameterListOwner  <PsiLambdaExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiLambdaExpression? {
        val newP = elementFactory.createExpressionFromText(text, null)
        if (newP !is PsiLambdaExpression) { return null }
        return newP
    }

    override public fun getTemplateFromElement(newP: PsiLambdaExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val text = newP.getText() ?: ""

        addParameterListToInsertPlaceMap(newP, insertPlaceMap)
        addCBtoInsertPlaceMap(newP.getBody(), BODY_TAG, insertPlaceMap, text)

        val contentRelation = getContentRelation(text, insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiLambdaExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareParameterListVariants(p, variants, context)
        preparePossibleCodeBlockPart(p.getBody(), BODY_TAG, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiLambdaExpression): Set<String> {
        val set = HashSet<String>()
        set.add(PARAMETER_LIST_TAG)
        addPossibleCodeBlockTag(set, p.getBody(), BODY_TAG)
        return set
    }

    override protected fun isTemplateSuitable(p: PsiLambdaExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}