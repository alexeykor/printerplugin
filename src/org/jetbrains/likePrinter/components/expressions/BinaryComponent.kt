package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiBinaryExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class BinaryComponent(
        printer: Printer
): PsiElementComponent <PsiBinaryExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiBinaryExpression, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiBinaryExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , LOperandOwner       <PsiBinaryExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ROperandOwner       <PsiBinaryExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , OperationSignOwner  <PsiBinaryExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    override public fun getTemplateFromElement(newP: PsiBinaryExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addLOperandToInsertPlaceMap     (newP, insertPlaceMap)
        addROperandToInsertPlaceMap     (newP, insertPlaceMap)
        addOperationSignToInsertPlaceMap(newP, insertPlaceMap)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiBinaryExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareLOperandVariants     (p, variants, context)
        prepareROperandVariants     (p, variants, context)
        prepareOperationSignVariants(p, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiBinaryExpression) = setOf(L_OPERAND_TAG, R_OPERAND_TAG, OPERATION_SIGN_TAG)
    override protected fun isTemplateSuitable(p: PsiBinaryExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}