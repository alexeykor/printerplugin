package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiReferenceExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.emptyFormatList
import org.jetbrains.likePrinter.components.QualifierOwner
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ReferenceComponent(
        printer: Printer
): PsiElementComponent <PsiReferenceExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiReferenceExpression, PsiTemplate<SmartInsertPlace>>
 , QualifierOwner      <PsiReferenceExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiReferenceExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    final val REFERENCE_NAME_TAG: String
        get() = "reference name"

    protected fun addReferenceNameToInsertPlaceMap(
              p: PsiReferenceExpression
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val refNameSIP = p.getReferenceNameElement()?.toSmartInsertPlace()
        if (refNameSIP == null) { return false }
        insertPlaceMap.put(REFERENCE_NAME_TAG, refNameSIP.shiftRight(delta))
        return true
    }

    protected fun prepareReferenceNameVariants(
              p: PsiReferenceExpression
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val refNameVariants = getReferenceNameVariants(p, context)
        if (refNameVariants.isEmpty()) { return }
        variants.put(REFERENCE_NAME_TAG, refNameVariants)
    }

    protected fun getReferenceNameVariants(
            p: PsiReferenceExpression, context: VariantConstructionContext
    ): FormatSet {
        val refNameElement = p.getReferenceNameElement()
        if (refNameElement == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(refNameElement, context)
        return variants
    }

    override public fun getTemplateFromElement(newP: PsiReferenceExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        if (!    addQualifierToInsertPlaceMap(newP, insertPlaceMap, 0)) { return null }
        if (!addReferenceNameToInsertPlaceMap(newP, insertPlaceMap, 0)) { return null }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiReferenceExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareQualifierVariants    (p, variants, context)
        prepareReferenceNameVariants(p, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiReferenceExpression) = setOf(QUALIFIER_TAG, REFERENCE_NAME_TAG)
    override protected fun isTemplateSuitable(p: PsiReferenceExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}