package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiThrowStatement
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ThrowComponent(
        printer: Printer
): StatementComponent  <PsiThrowStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , EmptyUpdateComponent<PsiThrowStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val EXCEPTION_TAG: String
        get() = "exception"

    override public fun getTemplateFromElement(
            newP: PsiThrowStatement
    ): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val exception = newP.getException()
        if (exception == null) { return null }
        val sip = exception.toSmartInsertPlace()
        insertPlaceMap.put(EXCEPTION_TAG, sip)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiThrowStatement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        val exceptionVariants = getExceptionVariants(p, context)
        if (exceptionVariants.isEmpty()) { return variants }
        variants.put(EXCEPTION_TAG, exceptionVariants)

        return variants
    }

    protected fun getExceptionVariants(p: PsiThrowStatement, context: VariantConstructionContext): FormatSet {
        val exception = p.getException()
        if (exception == null) { return printer.getEmptySet() }
        return printer.getVariants(exception, context)
    }

    override protected fun getTags(p: PsiThrowStatement) = setOf(EXCEPTION_TAG)

    override protected fun isTemplateSuitable(p: PsiThrowStatement, tmplt: PsiTemplate<SmartInsertPlace>) = true
}