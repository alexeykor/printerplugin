package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.templateBase.template.*
import com.intellij.psi.PsiWhileStatement
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElementFactory
import com.intellij.openapi.project.Project
import org.jetbrains.likePrinter.util.psiElement.*
import java.util.HashMap

import org.jetbrains.likePrinter.printer.Printer
import java.util.ArrayList
import org.jetbrains.likePrinter.util.base.*
import com.intellij.psi.PsiBlockStatement

/**
 * User: anlun
 */

public class WhileComponent(
        printer: Printer
): ConditionLoopComponent<PsiWhileStatement>(printer) {
}