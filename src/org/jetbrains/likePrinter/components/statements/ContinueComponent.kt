package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import com.intellij.psi.PsiContinueStatement

/**
 * User: anlun
 */
public class ContinueComponent(
        printer: Printer
): StatementComponent <PsiContinueStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , LabelIdentifierOwner<PsiContinueStatement> {
}