package org.jetbrains.likePrinter.components.statements

import com.intellij.psi.PsiLoopStatement
import com.intellij.psi.PsiExpression
import com.intellij.psi.PsiDoWhileStatement
import com.intellij.psi.PsiWhileStatement
import com.intellij.psi.PsiForStatement
import org.jetbrains.likePrinter.templateBase.template.*
import org.jetbrains.likePrinter.util.psiElement.*
import java.util.HashMap
import org.jetbrains.likePrinter.util.base.IncorrectPsiElementException

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.components.ConditionOwner
import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

/**
 * T must be PsiDoWhileStatement, PsiWhileStatement or PsiForStatement
 */
abstract public class ConditionLoopComponent<T: PsiLoopStatement>(
        printer: Printer
): LoopComponent <T>(printer)
 , ConditionOwner<T, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    override public fun getTemplateFromElement(newP: T): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val text = newP.getText() ?: ""

        val conditionSmartInsertPlace = getConditionInsertPlace(newP)
        if (conditionSmartInsertPlace == null && newP !is PsiForStatement) { return null }
        if (conditionSmartInsertPlace != null) {
            insertPlaceMap.put(CONDITION_TAG, conditionSmartInsertPlace)
        }

//        if (!addBodyToInsertPlaceMap(newP, insertPlaceMap)) { return null }
        addCBtoInsertPlaceMap(newP.getBody(), BODY_TAG, insertPlaceMap, text)

        if (!addToInsertPlaceMapSpecificElements(newP, insertPlaceMap)) { return null }

        val contentRelation = getContentRelation(text, insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    open protected fun addToInsertPlaceMapSpecificElements(
              p: T
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
    ): Boolean = true

    override protected fun prepareSubtreeVariants(p: T, context: VariantConstructionContext): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        try {
            prepareConditionPart(p, variants, context)
        } catch (e: IncorrectPsiElementException) {
            if (p !is PsiForStatement) { throw e }
        }
        prepareBodyPart     (p, variants, context)
        prepareSpecificParts(p, variants, context)

        return variants
    }

    override protected fun getTags(p: T): Set<String> {
        val set = HashSet<String>()
        set.add(CONDITION_TAG)
        addPossibleCodeBlockTag(set, p.getBody(), BODY_TAG)
        return set
    }

    open protected fun prepareSpecificParts(
              p: T, variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {}

}