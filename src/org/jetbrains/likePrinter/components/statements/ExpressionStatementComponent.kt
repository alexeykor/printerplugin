package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiExpressionStatement
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import java.util.ArrayList
import java.util.HashSet
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ExpressionStatementComponent(
        printer: Printer
): StatementComponent  <PsiExpressionStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , EmptyUpdateComponent<PsiExpressionStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val EXPRESSION_TAG: String
        get() = "expression"

    override public fun getTemplateFromElement(newP: PsiExpressionStatement): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset();

        val expression = newP.getExpression()
        insertPlaceMap.put(EXPRESSION_TAG, expression.toSmartInsertPlace().shiftRight(negShift))

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiExpressionStatement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        val expressionVariants = getExpressionVariants(p, context)
        variants.put(EXPRESSION_TAG, expressionVariants)

        return variants
    }

    protected fun getExpressionVariants(p: PsiExpressionStatement, context: VariantConstructionContext): FormatSet {
        val expression = p.getExpression()
        return printer.getVariants(expression, context)
    }

    override protected fun getTags(p: PsiExpressionStatement) = setOf(EXPRESSION_TAG)

    override protected fun isTemplateSuitable(p: PsiExpressionStatement, tmplt: PsiTemplate<SmartInsertPlace>): Boolean {
        val pText = p.getText() ?: ""
        return pText.endsWith(';') == tmplt.text.endsWith(';')
    }
    override public fun getVariants(p: PsiExpressionStatement, context: VariantConstructionContext): FormatSet {
        val variants = super<StatementComponent>.getVariants(p, context)
        if (printer.hasToUseMultipleExprStmtVariants()) { return variants }
        return variants.headSingleton()
    }
}