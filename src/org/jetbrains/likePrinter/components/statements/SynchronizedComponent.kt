package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.openapi.project.Project
import java.util.HashMap
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.util.base.IncorrectPsiElementException
import com.intellij.psi.PsiSynchronizedStatement
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.HashSet
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

public class SynchronizedComponent(
        printer: Printer
): StatementComponent  <PsiSynchronizedStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , EmptyUpdateComponent<PsiSynchronizedStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val LOCK_EXPRESSION_TAG = "lock expression"

    override public fun getTemplateFromElement(newP: PsiSynchronizedStatement): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val text = newP.getText() ?: ""

        val expression = newP.getLockExpression()
        if (expression == null) { return null }
        val expressionSIP = expression.toSmartInsertPlace()
        insertPlaceMap.put(LOCK_EXPRESSION_TAG, expressionSIP)

        addCBtoInsertPlaceMap(newP.getBody(), BODY_TAG, insertPlaceMap, text)

        val contentRelation = getContentRelation(text, insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiSynchronizedStatement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        val expressionVariants = getExpressionVariants(p, context)
        if (expressionVariants.isEmpty()) { return variants }
        variants.put(LOCK_EXPRESSION_TAG, expressionVariants)

        preparePossibleCodeBlockPart(p.getBody(), BODY_TAG, variants, context)

        return variants
    }

    protected fun getExpressionVariants(p: PsiSynchronizedStatement, context: VariantConstructionContext): FormatSet {
        val expression = p.getLockExpression()
        if (expression == null) { return printer.getEmptySet() }
        return printer.getVariants(expression, context)
    }

    override protected fun getTags(p: PsiSynchronizedStatement): Set<String> {
        val set = HashSet<String>()
        if (hasElement(p.getLockExpression())) { set.add(LOCK_EXPRESSION_TAG) }
        addPossibleCodeBlockTag(set, p.getBody(), BODY_TAG)
        return set
    }

    override protected fun isTemplateSuitable(p: PsiSynchronizedStatement, tmplt: PsiTemplate<SmartInsertPlace>) = true
}