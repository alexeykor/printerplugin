package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiBreakStatement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.components.EmptyUpdateComponent

/**
 * User: anlun
 */
public class BreakComponent(
        printer: Printer
): StatementComponent <PsiBreakStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , LabelIdentifierOwner<PsiBreakStatement> {
}