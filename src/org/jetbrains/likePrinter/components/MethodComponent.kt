package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiMethod
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiElementFactory
import com.intellij.openapi.project.Project
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.base.IncorrectPsiElementException
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.util.psiElement.getFillConstant
import org.jetbrains.likePrinter.util.psiElement.getNotNullTextRange

import java.util.HashSet
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent.CodeBlockSpecialTemplateInsertPlace
import com.intellij.psi.PsiCodeBlock
import com.intellij.psi.PsiBlockStatement
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class MethodComponent(
        printer: Printer
): PsiElementComponent   <PsiMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , EmptyUpdateComponent  <PsiMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ModifierListOwner     <PsiMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , NameOwner             <PsiMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , TypeParameterListOwner<PsiMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ParameterListOwner    <PsiMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ReturnTypeOwner       <PsiMethod, PsiTemplate<SmartInsertPlace>>
 , ThrowsListOwner       <PsiMethod, PsiTemplate<SmartInsertPlace>> {

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiMethod? {
        val newP = elementFactory.createMethodFromText(text, null)
        if (newP !is PsiMethod) { return null }
        return newP
    }

    override public fun getTemplateFromElement(newP: PsiMethod): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val text = newP.getText() ?: ""

        addModifierListToInsertPlaceMap(newP, insertPlaceMap)
        if (!addNameToInsertPlaceMap(newP, insertPlaceMap)) { return null }
        addParameterListToInsertPlaceMap(newP, insertPlaceMap)
        addTypeParameterListToInsertPlaceMap(newP, insertPlaceMap)
        addCBtoInsertPlaceMap(newP.getBody(), BODY_TAG, insertPlaceMap, text)

        addReturnTypeToInsertPlaceMap(newP, insertPlaceMap, 0)
        addThrowsListToInsertPlaceMap(newP, insertPlaceMap, 0)

        val contentRelation = getContentRelation(text, insertPlaceMap)

        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
                    p: PsiMethod
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareModifierListVariants(p, variants, context)

        val returnTypeVariants = getReturnTypeVariants(p, context)
        if (!returnTypeVariants.isEmpty()) {
            variants.put(RETURN_TYPE_TAG, returnTypeVariants)
        }

        prepareNameVariants         (p, variants, context)
        prepareParameterListVariants(p, variants, context)

        val throwsListVariants = getThrowsListVariants(p, context)
        if (!throwsListVariants.isEmpty()) {
            variants.put(THROWS_LIST_TAG, throwsListVariants)
        }

        prepareTypeParameterListVariants(p, variants, context)
        preparePossibleCodeBlockPart(p.getBody(), BODY_TAG, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiMethod): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getModifierList())) { set.add(MODIFIER_LIST_TAG) }
        if (p.getReturnType() != null)       { set.add(  RETURN_TYPE_TAG) }
        set.add(NAME_TAG)
        if (hasElement(p.    getParameterList())) { set.add(     PARAMETER_LIST_TAG) }
        if (hasElement(p.       getThrowsList())) { set.add(        THROWS_LIST_TAG) }
        if (hasElement(p.getTypeParameterList())) { set.add(TYPE_PARAMETER_LIST_TAG) }
        addPossibleCodeBlockTag(set, p.getBody(), BODY_TAG)

        return set
    }

    override protected fun isTemplateSuitable(p: PsiMethod, tmplt: PsiTemplate<SmartInsertPlace>): Boolean = true
}